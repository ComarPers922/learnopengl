#pragma once

#include <string>
#include <iostream>
#include <fstream>

#include "../../nclgl/Mesh.h"

constexpr auto RAW_WIDTH = 512;
constexpr auto RAW_HEIGHT = 512;

constexpr auto HEIGHTMAP_X = 16.0f;
constexpr auto HEIGHTMAP_Z = 16.0f;
constexpr auto HEIGHTMAP_Y = 30.25f;

constexpr auto HEIGHTMAP_TEX_X = 1.0f / 16.0f;
constexpr auto HEIGHTMAP_TEX_Z = 1.0f / 16.0f;

class HeightMap : public Mesh
{
public:
	HeightMap(std::string name);
	~HeightMap();
};

