#pragma once
#include "OGLRenderer.h"

using std::string;
using std::ifstream;
using std::cout;
using std::endl;
using std::getline;

enum Shader_Type
{
	SHADER_VERTEX, SHADER_FRAGMENT, SHADER_GEOMETRY, MAX_SHADER
};

class Shader
{
public :
	Shader(string vFile, string fFile, string gFile = "");
	~Shader();

	GLuint GetProgram()
	{
		return program;
	}
	bool LinkProgram();

protected:
	void SetDefaultAttributes();
	bool LoadShaderFile(string from, string &into);
	GLuint GenerateShader(string from, GLenum type);

	GLuint objects[3];
	GLuint program;

	bool loadFailed;
};

