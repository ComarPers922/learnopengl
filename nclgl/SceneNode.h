#pragma once
#include "Matrix4.h"
#include "Vector3.h"
#include "Vector4.h"
#include "Mesh.h"
#include <vector>

class SceneNode
{
public:
	SceneNode(Mesh* m = NULL, Vector4 color = Vector4(1, 1, 1, 1));
	~SceneNode();

	void SetTransform(const Matrix4& matrix)
	{
		transform = matrix;
	}

	const Matrix4& GetTransform() const
	{
		return transform;
	}

	Matrix4 GetWorldTransform() const
	{
		return worldTransform;
	}

	Vector4 GetColor() const
	{
		return color;
	}
	void SetColor(const Vector4& c)
	{
		color = c;
	}

	Vector3 GetModelScale() const
	{
		return modelScale;
	}
	void SetModelScale(const Vector3& s)
	{
		modelScale = s;
	}
	void SetModelScale(float x, float y, float z)
	{
		SetModelScale(Vector3(x, y, z));
	}

	Mesh* GetMesh () const
	{
		return mesh;
	}
	void SetMesh(Mesh* m)
	{
		mesh = m;
	}

	void AddChild(SceneNode* s);

	virtual void Update(float msec);
	virtual void Draw(const OGLRenderer& r);

	std::vector<SceneNode*>::const_iterator GetChildIteratorStart()
	{
		return children.begin();
	}
	std::vector<SceneNode*>::const_iterator GetChildIteratorEnd()
	{
		return children.end();
	}

	float GetBoundingRadius() const
	{
		return boundingRadius;
	}
	void SetBoundingRadius(float f)
	{
		boundingRadius = f;
	}

	float GetCameraDistance() const
	{
		return distanceFromCamera;
	}
	void SetCameraDistance(float f)
	{
		distanceFromCamera = f;
	}

protected:
	SceneNode* parent;
	Mesh* mesh;
	Matrix4 worldTransform;
	Matrix4 transform;
	Vector3 modelScale;
	Vector4 color;
	std::vector <SceneNode*> children;

	float distanceFromCamera;
	float boundingRadius;
};

