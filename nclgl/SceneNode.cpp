#include "SceneNode.h"

SceneNode::SceneNode(Mesh* mesh, Vector4 color)
{
	this->mesh = mesh;
	this->color = color;
	parent = NULL;
	modelScale = Vector3(1, 1, 1);
}

SceneNode::~SceneNode()
{
	for (unsigned int i = 0; i < children.size(); i++)
	{
		delete children[i];
	}
}

void SceneNode::AddChild(SceneNode* s)
{
	children.push_back(s);
	s->parent = this;
}

void SceneNode::Draw(const OGLRenderer& r)
{
	if (mesh)
	{
		mesh->Draw();
	}
}

void SceneNode::Update(float msec)
{
	if (parent)
	{
		worldTransform = parent->worldTransform * transform;
	}
	else
	{
		worldTransform = transform;
	}
	for (auto* item : children)
	{
		item->Update(msec);
	}
}