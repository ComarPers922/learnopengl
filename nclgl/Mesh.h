#pragma once
#include "OGLRenderer.h"

enum MeshBuffer
{
	VERTEX_BUFFER, COLOR_BUFFER, TEXTURE_BUFFER, NORMAL_BUFFER, TANGENT_BUFFER, INDEX_BUFFER, MAX_BUFFER
};

class Mesh
{
public:
	Mesh();
	~Mesh();

	virtual void Draw();
	static Mesh* GenerateTriangle();

	void SetTexture(GLuint tex)
	{
		texture = tex;
	}
	GLuint GetTexture()
	{
		return texture;
	}

	void SetBumpMap(GLuint tex)
	{
		bumpTexture = tex;
	}
	GLuint GetBumpMap()
	{
		return bumpTexture;
	}

	static Mesh* GenerateQuad();
	

protected:
	void BufferData();

	Vector3* normals;

	GLuint arrayObject;
	GLuint bufferObject[MeshBuffer::MAX_BUFFER];
	GLuint numVertices;
	GLuint type;

	GLuint texture;
	Vector2* textureCoords;

	Vector3* vertices;
	Vector4* colors;

	GLuint numIndices;
	unsigned int* indices;

	Vector3 GenerateTangent(const Vector3 &a, const Vector3 &b,
													const Vector3 &c, const Vector2 &ta,
													const Vector2 &tb, const Vector2 &tc);
	void GenerateNormals();
	void GenerateTangents();
	Vector3* tangents;
	GLuint bumpTexture;
};

