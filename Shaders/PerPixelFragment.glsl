#version 150 core

uniform sampler2D diffuseTex;

uniform vec3 cameraPos;
uniform vec4 lightColor;
uniform vec3 lightPos;
uniform float lightRadius;
uniform float intensity;

in Vertex
{
    vec4 color;
    vec2 texCoord;
    vec3 normal;
    vec3 worldPos;
    vec3 fragPos;
} IN;

out vec4 fragColor;

void main()
{
    vec4 diffuse = texture(diffuseTex, IN.texCoord);

    vec3 incident = normalize(lightPos - IN.worldPos);
    float lambert = max(0.0, dot(incident, IN.normal));

    float dist = length(lightPos - IN.worldPos);
    float atten = 1.0 - clamp(dist / lightRadius, 0.0, 1.0);
    vec3 viewDir = normalize(cameraPos - IN.worldPos);
    vec3 halfDir = normalize(incident + viewDir);

    float rFactor = max(0.0, dot(halfDir, IN.normal));
    float sFactor = pow(rFactor, 50.0);

    vec3 color = diffuse.rgb * lightColor.rgb;
    color += lightColor.rgb * sFactor * 0.33;
    fragColor = vec4(color * atten * lambert, diffuse.a);
    fragColor.rgb += diffuse.rgb * lightColor.rgb * 0.1;
}