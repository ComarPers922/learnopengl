#version 150 core

uniform sampler2D diffuseTex;
uniform sampler2D bumpTex;

uniform vec3 cameraPos;
uniform vec4 lightColor;
uniform vec3 lightPos;
uniform float lightRadius;

in Vertex
{
    vec4 color;
    vec2 texCoord;
    vec3 normal;
    vec3 tangent;
    vec3 binormal;
    vec3 worldPos;
} IN;

out vec4 fragColor;

void main()
{
    vec4 diffuse = texture(diffuseTex, IN.texCoord);
    mat3 TBN = mat3(IN.tangent, IN.binormal, IN.normal);
    vec3 normal = normalize(TBN * (texture(bumpTex, IN.texCoord).rgb * 2.0 - 1.0));

    vec3 incident = normalize(lightPos - IN.worldPos);
    float lambert = max(0.0, dot(incident, normal));
    float dist = length(lightPos - IN.worldPos);
    float atten = 1.0 - clamp(dist / lightRadius, 0.0, 1.0);

    vec3 viewDir = normalize(cameraPos - IN.worldPos);
    vec3 halfDir = normalize(incident + viewDir);

    float rFactor = max(0.0, dot(halfDir, normal));
    float sFactor = pow(rFactor, 33.0);

    vec3 color = diffuse.rgb * lightColor.rgb;
    color += (lightColor.rgb * sFactor) * 0.33;
    fragColor = vec4(color * atten * lambert, diffuse.a);
    fragColor.rgb += (diffuse.rgb * lightColor.rgb) * 0.1;
}