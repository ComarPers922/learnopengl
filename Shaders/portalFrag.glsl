#version 150 core
uniform sampler2D diffuseTex;
uniform sampler2D bumpTex;

in Vertex 
{
	vec2 	texCoord;
	vec4 	color;
} IN;

out vec4 gl_FragColor;

void main(void)
{
	gl_FragColor = texture(diffuseTex, IN.texCoord);
}