#version 150 core

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform mat4 textureMatrix[2];


in vec3 position;
in vec4 color;
in vec3 normal;
in vec2 texCoord;

out Vertex
{
    vec4 color;
    vec2 texCoord;
    vec3 normal;
    vec3 worldPos;
    vec3 fragPos;
    vec4 shadowProj[2];
} OUT;

void main()
{
    OUT.color = color;
   
    mat3 normalMatrix = transpose(inverse(mat3(modelMatrix)));

    OUT.normal = normalize(normalMatrix * normalize(normal));
    OUT.worldPos = (modelMatrix * vec4(position, 1)).xyz;
    OUT.fragPos = vec3(modelMatrix * vec4(position, 1.0));
    OUT.texCoord = texCoord;
    for(int i = 0; i < 2; i ++)
    {    
        OUT.shadowProj[i] = (textureMatrix[i] * vec4(position + (normal * 1.5), 1));
    }
    gl_Position = (projMatrix * viewMatrix * modelMatrix) * vec4(position, 1.0);
}