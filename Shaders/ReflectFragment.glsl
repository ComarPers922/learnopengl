#version 150 core

uniform sampler2D diffuseTex;
uniform sampler2D bumpTex;
uniform samplerCube cubeTexNight;
uniform samplerCube cubeTexDay;

uniform vec4 lightColor;
uniform vec3 lightPos;
uniform vec3 cameraPos;
uniform float lightRadius;
uniform float offsetX;
uniform float waterOffsetX;
uniform float TForLerp;

vec4 colorLerp(vec4 from, vec4 to, float t)
{
	t = clamp(t, 0, 1);
	return (to - from) * t + from;
}


mat4 rotationY(in float angle) 
{
	return mat4(cos(angle), 0, sin(angle), 0,
			 	0, 1.0, 0, 0,
				-sin(angle), 0, cos(angle), 0,
				0, 0, 0, 1);
}

mat4 rotationX(in float angle) 
{
	return mat4(1.0, 0, 0, 0,
			 	0, cos(angle), -sin(angle), 0,
				0, sin(angle), cos(angle), 0,
				0, 0, 0, 1);
}

mat4 rotationZ(in float angle) 
{
	return mat4(cos(angle),-sin(angle),0,0,
			 	sin(angle),cos(angle),0,0,
				0,0,1,0,
				0,0,0,1);
}

in Vertex
{
    vec4 color;
    vec2 texCoord;
    vec3 normal;
    vec3 tangent;
    vec3 binormal;
    vec3 worldPos;
} IN;

out vec4 fragColor;

void main()
{
    mat3 TBN = mat3(IN.tangent, IN.binormal, IN.normal);
    vec3 normal = normalize(TBN * (texture(bumpTex, IN.texCoord + vec2(offsetX, 0)).rgb * 2.0 - 1.0));

    vec4 diffuse = texture(diffuseTex, IN.texCoord + vec2(offsetX, 0) + IN.texCoord) * IN.color;
    vec3 incident = normalize(IN.worldPos - cameraPos);
    vec3 incidentL = normalize(lightPos - IN.worldPos);
    float lambert = max(0.0, dot(incidentL, normal));

    vec3 viewDir = normalize(cameraPos - IN.worldPos);
    vec3 halfDir = normalize(incidentL + viewDir);

    float rFactor = max(0.0, dot(halfDir, normal));
    float sFactor = pow(rFactor, 33.0);

    float dist = length(lightPos - IN.worldPos);
    float atten = 1.0 - clamp(dist / lightRadius, 0.2, 1.0);

    vec4 reflectionDay = texture(cubeTexDay, (vec4(reflect(incident, normalize(IN.normal)), 1.0) * rotationY(waterOffsetX)).xyz);
    vec4 reflectionNight = texture(cubeTexNight, (vec4(reflect(incident, normalize(IN.normal)), 1.0) * rotationY(waterOffsetX) * rotationX(waterOffsetX / 2)).xyz);
    vec4 reflection = colorLerp(reflectionNight, reflectionDay, TForLerp);

    fragColor = (lightColor * lambert * diffuse  * atten) * (diffuse + reflection);
    fragColor += vec4(1, 1, 1, 0.7) * (diffuse + reflection + lambert * 0.1) * 0.5;
}