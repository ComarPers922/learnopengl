# version 150 core

 

 uniform sampler2D diffuseTex ;
uniform sampler2D bumpTex ;
 uniform vec3 cameraPos ;

 

 uniform vec4 lightColour ;
 uniform vec3 lightPos ;
 uniform float lightRadius ;

 

 uniform samplerCube cubeTex ;
 in Vertex {
 vec4 color ;
 vec2 texCoord ;
 vec3 normal ;
 vec3 tangent; 
 vec3 binormal; 
 vec3 worldPos ;
 } IN ;

 

 out vec4 fragColour ;
void main ( void ) {
vec2 tex = IN.texCoord;
    
     mat3 TBN = mat3 ( IN . tangent , IN . binormal , IN . normal );
vec3 normal = normalize ( TBN * ( texture ( bumpTex ,tex). rgb * 2.0 - 1.0));

 


 vec4 diffuse = texture ( diffuseTex , IN . texCoord ) * IN . color ;

 

 vec3 incidentReflect = normalize ( IN . worldPos - cameraPos );

 

 vec3 incident = normalize ( lightPos - IN . worldPos );
vec3 viewDir = normalize ( cameraPos - IN . worldPos );
     vec3 halfDir = normalize ( incident + viewDir );

 

float lambert = max (0.0 , dot ( incident , normal ));
    float rFactor = max (0.0 , dot ( halfDir , normal ));
    float sFactor = pow ( rFactor , 33.0 );

 


 float dist = length ( lightPos - IN . worldPos );
 float atten = 1.0 - clamp ( dist / lightRadius , 0.2 , 1.0);

 


vec3 colour = ( diffuse . rgb * lightColour . rgb );
     colour += ( lightColour . rgb * sFactor ) * 0.33;

 

 vec4 reflection = texture ( cubeTex ,
 reflect (incidentReflect, normalize ( IN . normal )));

 

fragColour = vec4 ( colour * atten * lambert , diffuse . a );
     fragColour . rgb += ( diffuse . rgb * lightColour . rgb ) * 0.1;

 

    fragColour = fragColour + (lightColour * diffuse * atten) * (diffuse + reflection);
    //fragColour = vec4(1.0f, 0.0f, 0.0f,1.0f);
    //fragColour = vec4(normal,1.0);
 }