#version 150 core
uniform sampler2D diffuseTex;
uniform sampler2D bumpTex;

in Vertex 
{
	vec2 	texCoord;
	vec4 	color;
} IN;

out vec4 gl_FragColor;

void main(void)
{
	gl_FragColor = mix(texture(diffuseTex, IN.texCoord), IN.color, 0.0);
	gl_FragColor.a = 0.7;
}