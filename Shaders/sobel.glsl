#version 150 core

uniform sampler2D diffuseTex;
uniform vec2 pixelSize;
uniform int isVertical;

in Vertex
{
    vec2 texCoord;
    vec4 color;
} IN;

out vec4 fragColor;

const float weights[5] = float[](0.12, 0.22, 0.32, 0.22, 0.12);

mat3 gx = mat3(-1, 0, 1,
            -2, 0, 2,
            -1, 0, 1);
mat3 gy = mat3(-1, -2, -1, 
            0, 0, 0, 
            1, 2, 1);

void main()
{
    float a = texture2D (diffuseTex, IN.texCoord.xy).a;
    mat3 g;
    if (isVertical == 1)
    {
        g = gy;
    }
    else
    {
        g = gx;
    }
    for (int i = 0; i <= 2; i++)
    {
        for (int j = 0; j <= 2; j++)
        {    
            float px = pixelSize.x * (i-1);
	        float py = pixelSize.y * (j-1);
            vec3 tmp = texture2D (diffuseTex, IN.texCoord.xy + vec2(pixelSize.x * (i-1), pixelSize.y * (j-1))).rgb;
            fragColor += vec4(tmp * g[i][j], 0);
        }
    }
    fragColor.rgb = sqrt(fragColor.rgb);
    fragColor.a = a;
}