#version 150 core
uniform sampler2D diffuseTex;

uniform float snowAmbient;


in Vertex 
{
	vec2 	texCoord;
	vec4 	color;
} IN;

out vec4 FragColor;

void main()
{
	FragColor = texture(diffuseTex, IN.texCoord);
	FragColor.rgb = IN.color.rgb;
	float colorOffset = snowAmbient;
	FragColor.rgb +=  vec3(colorOffset, colorOffset, colorOffset);
}