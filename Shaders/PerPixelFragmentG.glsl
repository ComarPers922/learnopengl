#version 150 core

uniform sampler2D diffuseTex;

uniform vec3 cameraPos;
uniform vec4 lightColor[2];
uniform vec3 lightPos[2];
uniform float lightRadius[2];
uniform float intensity[2];
uniform vec4 ambientColor;
uniform float ambientIntensity;
uniform float specularStrength;

uniform sampler2DShadow shadowTex[2];

in Vertex
{
    vec4 color;
    vec2 texCoord;
    vec3 normal;
    vec3 worldPos;
    vec3 fragPos;
    vec4 shadowProj[2];
} IN;

out vec4 fragColor;

void main()
{
    vec4 diffuse = texture(diffuseTex, IN.texCoord);
    for(int i = 0; i < 2; i ++)
    {
        // Simple diffuse light
        vec3 lightDir = normalize(lightPos[i] - IN.fragPos);
        vec3 incident = normalize(lightPos[i] - IN.worldPos);
        float lambert = max(0.0, dot(incident, IN.normal));

        float dist = length(lightPos[i] - IN.worldPos);
        float atten = 1.0 - clamp(dist / lightRadius[i], 0.0, 1.0);
        vec3 viewDir = normalize(cameraPos - IN.worldPos);
        vec3 halfDir = normalize(incident + viewDir);

        float rFactor = max(0.0, dot(halfDir, IN.normal));
        float aFactor = pow(rFactor, 50.0);

        float shadow = 1.0;
        if(IN.shadowProj[i].w > 0.0)
        {
            shadow = textureProj(shadowTex[i], IN.shadowProj[i]);
        } 
        
        lambert *= shadow;

        vec3 color = diffuse.rgb * lightColor[i].rgb;
        color += lightColor[i].rgb * aFactor * 0.33;
        fragColor += vec4(color * atten * lambert, diffuse.a)* (clamp(intensity[i], 0, 2) * 2);
        fragColor.rgb += diffuse.rgb * lightColor[i].rgb * 0.1;
        
        // Specular
        vec3 reflectDir = reflect(-lightDir, IN.normal);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
        vec3 specular = specularStrength * spec * lightColor[i].rgb;
        fragColor.rgb += specular * lightColor[i].rgb;
    }
    // Ambient light
    fragColor += ambientColor * diffuse * clamp(ambientIntensity, 0, 1);
    fragColor.a = IN.color.a;
}