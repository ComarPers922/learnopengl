#include "Camera.h"

namespace T2Camera
{
	void Camera::UpdateCamera(float msec)
	{
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_CONTROL))
		{
			pitch -= (Window::GetMouse()->GetRelativePosition().y);
			yaw -= (Window::GetMouse()->GetRelativePosition().x);
		}

		pitch = min(pitch, 90.0f);
		pitch = max(pitch, -90.0f);

		if (yaw < 0)
		{
			yaw += 360.0f;
		}
		if (yaw > 360.0f)
		{
			yaw -= 360.0f;
		}

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_T))
		{
			position += Matrix4::Rotation(yaw, Vector3(0.0f, 1.0f, 0.0f)) * Vector3(0.0f, 0.0f, -1.0f) * msec;
		}
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_G))
		{
			position -= Matrix4::Rotation(yaw, Vector3(0.0f, 1.0f, 0.0f)) * Vector3(0.0f, 0.0f, -1.0f) * msec;
		}

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_F))
		{
			position += Matrix4::Rotation(yaw, Vector3(0.0f, 1.0f, 0.0f)) *
				Vector3(-1.0f, 0.0f, 0.0f) * msec;
		}
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_H))
		{
			position -= Matrix4::Rotation(yaw, Vector3(0.0f, 1.0f, 0.0f)) *
				Vector3(-1.0f, 0.0f, 0.0f) * msec;
		}

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_SHIFT))
		{
			position.y += msec;
		}
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_SPACE))
		{
			position.y -= msec;
		}
	}

	Matrix4 Camera::BuildViewMatrix()
	{
		return Matrix4::Rotation(-pitch, Vector3(1, 0, 0)) *
			Matrix4::Rotation(-yaw, Vector3(0, 1, 0)) *
			Matrix4::Translation(-position);
	}
}