#pragma once

#include "../../nclgl/OGLRenderer.h"
#include "Camera.h"

class Renderer: public OGLRenderer
{
public:
	Renderer(Window& parent);
	virtual ~Renderer();

	virtual void RenderScene();

	void SwitchToPerspective();
	void SwitchToOrthographic();

	inline void SetScale(float s)
	{
		scale = s;
	}

	inline void SetRotation(float r)
	{
		rotation = r;
	}

	inline void SetPosition(Vector3 p)
	{
		position = p;
	}

	virtual void UpdateScene(float mecs);
protected:
	Mesh* triangle;

	float scale;
	float rotation;
	Vector3 position;
	T2Camera::Camera* camera;
};

