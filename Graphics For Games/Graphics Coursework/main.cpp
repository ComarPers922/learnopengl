#include "../../NCLGL/window.h"
#include "Renderer.h"

#pragma comment(lib, "nclgl.lib")

int main() {
	Window w("Newcastle-under-Galaxy", 1920, 1200, true);
	if (!w.HasInitialized())
	{
		return -1;
	}

	Renderer renderer(w);//This handles all the boring OGL 3.2 initialization stuff, and sets up our tutorial!
	if (!renderer.HasInitialized()) 
	{
		return -1;
	}

	w.LockMouseToWindow(true);
	w.ShowOSPointer(false);

	while (w.UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE))
	{
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_M))
		{
			renderer.SetShowMinimap(true);
		}
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_N))
		{
			renderer.SetShowMinimap(false);
		}
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_L))
		{
			renderer.LockCamera();
		}
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_U))
		{
			renderer.UnlockCamera();
		}
		if (Window::GetKeyboard()->KeyDown(KEYBOARD_R))
		{
			renderer.ReturnToWorld0();
		}
		for (int i = 0; i < 5; i++)
		{
			if (Window::GetKeyboard()->KeyDown((KeyboardKeys)(KEYBOARD_0 + i)))
			{
				renderer.SpaceJump(i);
			}
		}
		renderer.UpdateScene(w.GetTimer()->GetTimedMS());
		renderer.RenderScene();
	}

	return 0;
}