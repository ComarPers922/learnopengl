#include "SwordNode.h"


SwordNode::SwordNode()
{
	OBJMesh* sword_obj = new OBJMesh();
	sword_obj->LoadOBJMesh(MESHDIR"chr_sword-2.obj");
	sword_obj->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"chr_sword.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
	mesh = sword_obj;

	SetModelScale(Vector3(20, 20, 20));
	SetTransform(Matrix4::Translation(Vector3(-150, 20, 60.0)));
}

SwordNode::~SwordNode()
{

}

void SwordNode::Update(float msec)
{
	SceneNode::Update(msec);
}