#include "TreeNode.h"

TreeNode::TreeNode()
{
	OBJMesh* tree_obj = new OBJMesh();
	tree_obj->LoadOBJMesh(MESHDIR"tree.obj");
	tree_obj->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"tree.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
	mesh = tree_obj;
}

TreeNode::~TreeNode()
{
}

void TreeNode::Update(float msec)
{
	SceneNode::Update(msec);
}
