#pragma once
#include "../../nclgl/Vector2.h"
#include "../../nclgl/Vector3.h"
#include <cstdlib>
#include <ctime>
#include <climits>

class SimpleMath
{
public:
	static float Lerp(float left, float right, float t)
	{
		t = Clamp(t, 0, 1);
		return (right - left) * t + left;
	}

	static Vector2 Lerp(Vector2 left, Vector2 right, float t)
	{
		t = Clamp(t, 0, 1);
		return (right - left) * t + left;
	}

	static Vector3 Lerp(Vector3 left, Vector3 right, float t)
	{
		t = Clamp(t, 0, 1);
		return (right - left) * t + left;
	}

	static float Clamp(float val, float min, float max)
	{
		if (val < min)
		{
			return min;
		}
		else if (val > max)
		{
			return max;
		}
		return val;
	}
	static void SRand()
	{
		srand(time(0));
	}
	static float Random(float min, float max)
	{
		float result = 0;
		float randNum = rand() / (float)RAND_MAX;
		result = (max - min) * randNum + min;
		return result;
	}
};

