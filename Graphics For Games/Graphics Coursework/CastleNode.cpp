#include "CastleNode.h"

CastleNode::CastleNode()
{
	OBJMesh* castle_obj = new OBJMesh();
	castle_obj->LoadOBJMesh(MESHDIR"castle.obj");
	castle_obj->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"castle.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
	mesh = castle_obj;

	SetModelScale(Vector3(20, 20, 20));
	SetTransform(Matrix4::Translation(Vector3(-150, 20, 60.0)));
}

CastleNode::~CastleNode()
{
}

void CastleNode::Update(float msec)
{
	SceneNode::Update(msec);
}
