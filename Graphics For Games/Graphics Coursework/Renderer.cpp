#include "Renderer.h"

Renderer::Renderer(Window& parent) : OGLRenderer(parent)
{
	SimpleMath::SRand();
	quad = Mesh::GenerateQuad();
	waterQuad = Mesh::GenerateQuad();
	minimapQuad = Mesh::GenerateQuad();
	portalQuad = Mesh::GenerateQuad();
	snowflakeMesh = new Mesh();
	snowflakeTexture = SOIL_load_OGL_texture(TEXTUREDIR"snowflake.tga", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);
	snowflakeMesh = Mesh::GenerateQuad();
	snowflakeMesh->SetTexture(snowflakeTexture);

	snowFlakePositions = new Vector3[NUMBER_OF_PARTICLES];
	snowColors = new Vector4[NUMBER_OF_PARTICLES];
	snowAmbients = new SnowAmbient[NUMBER_OF_PARTICLES];
	snowOffsets = new Vector3[NUMBER_OF_PARTICLES];
	for (int i = 0; i < NUMBER_OF_PARTICLES; i++)
	{	
		float offsetX = SimpleMath::Random(-SNOW_OFFSET, SNOW_OFFSET);
		float offsetZ = SimpleMath::Random(-SNOW_OFFSET, SNOW_OFFSET);
		float offsetY = SimpleMath::Random(2000, 8000);
		snowFlakePositions[i] = Vector3(WORLD_ORIGIN_X + offsetX, offsetY, WORLD_ORIGIN_Z + offsetZ);
		snowColors[i] = Vector4(SimpleMath::Random(0.5, 1), SimpleMath::Random(0.5, 1), SimpleMath::Random(0.5, 1),1);
		snowAmbients[i].ambient = 0.0f;
		snowAmbients[i].isIncreasing = (SimpleMath::Random(0.0f, 1.0f) >= 0.5f);
		snowOffsets[i] = Vector3(SimpleMath::Random(-2, 2),
														SimpleMath::Random(SNOW_SPEED, - 2),
														SimpleMath::Random(-2, 2));
	}

	terrain = new HeightMap(TEXTUREDIR"terrain512.raw");
	terrain->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"lowpolyterrain.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));

	terrain->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"Barren RedsDOT3.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	
	waterQuad->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"water.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	waterQuad->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"water_normal.tga", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));

	minimapQuad->SetTexture(minimapTex);

	camera = new Camera(-8.0f, 40.0f, Vector3(-200.0f, 50.0f, 250.0f), 2.0f);
	camera->SetCameraStatus(cameraStatuses[0].yaw, cameraStatuses[0].pitch, cameraStatuses[0].pos);
	camera->SetIsLocked(true);

	minimapCamera = new Camera(-90, 0, Vector3(WORLD_ORIGIN_X, 8000, WORLD_ORIGIN_Z));
	minimapCamera->SetIsLocked(true);


	portalCamera.SetIsLocked(true);

	lights = new Light[NUMBER_OF_LIGHTS];
	SHADOW_LIGHT =  Light(Vector3(WORLD_ORIGIN_X + 1500, WORLD_ORIGIN_Y + 1000, WORLD_ORIGIN_Z + 500), Vector4(1, 0.9568627f, 0.8392157f, 1), 3000.0f, 0.5f);
	lights[1] = Light(Vector3(WORLD_ORIGIN_X, WORLD_ORIGIN_Y + 1000, WORLD_ORIGIN_Z), Vector4(1, 0.9568627f, 0.8392157f, 1), 5500.0f, 0.2f);

	sceneShader = new Shader(SHADERDIR"shadowlightshaderVert.glsl",
															SHADERDIR"shadowlightshaderFrag.glsl");
	shadowShader = new Shader(SHADERDIR"shadowVert.glsl",
															SHADERDIR"shadowFrag.glsl");
	skyboxShader = new Shader(SHADERDIR"SkyboxVertex.glsl",
															SHADERDIR"SkyboxFragment.glsl");
	terrainShader = new Shader(SHADERDIR"BumpVertex.glsl", 
															SHADERDIR"BumpFragment.glsl");
	waterShader = new Shader(SHADERDIR"NormalPixelVertex.glsl",
															SHADERDIR"ReflectFragment.glsl");
	minimapShader = new Shader(SHADERDIR"TexturedVertex.glsl",
																SHADERDIR"TexturedFragment.glsl");
	snowShader = new Shader(SHADERDIR"snowVertex.glsl", 
		SHADERDIR"snowFragment.glsl");
	portalShader = new Shader(SHADERDIR"TexturedVertex.glsl", SHADERDIR"portalFrag.glsl");

	cubeMapNight = SOIL_load_OGL_cubemap(TEXTUREDIR"CosmicCoolCloudLeft.tga", TEXTUREDIR"CosmicCoolCloudRight.tga",
		TEXTUREDIR"CosmicCoolCloudBottom.tga", TEXTUREDIR"CosmicCoolCloudTop.tga",
		TEXTUREDIR"CosmicCoolCloudBack.tga", TEXTUREDIR"CosmicCoolCloudFront.tga",
		SOIL_LOAD_RGBA, SOIL_CREATE_NEW_ID, 0);
	cubeMapDay = SOIL_load_OGL_cubemap(TEXTUREDIR"FluffballDayRight.tga", TEXTUREDIR"FluffballDayLeft.tga",
		TEXTUREDIR"FluffballDayTop.tga", TEXTUREDIR"FluffballDayBottom.tga",
		TEXTUREDIR"FluffballDayBack.tga", TEXTUREDIR"FluffballDayFront.tga",
		SOIL_LOAD_RGBA, SOIL_CREATE_NEW_ID, 0);
	rootNode = new RootNode();

	knight = new KnightNode();
	sword = new SwordNode();
	Newcastle = new CastleNode();
	tree = new TreeNode();
	castleRoom = new CastleRoomNode();

	knight->SetModelScale(Vector3(50, 50, 50));
	knight->SetTransform(Matrix4::Translation(Vector3(900, 200, -900)));
	sword->SetModelScale(Vector3(50, 50, 50));
	sword->SetTransform(Matrix4::Translation(Vector3(1000, 200, -900)));
	Newcastle->SetModelScale(Vector3(500, 500, 500));
	Newcastle->SetTransform(Matrix4::Translation(Vector3(1000, 550, -1500)));
	tree->SetModelScale(250, 350, 250);
	tree->SetTransform(Matrix4::Translation(Vector3(1500, 0, 600)));
	castleRoom->SetModelScale(200, 200, 200);
	castleRoom->SetTransform(Matrix4::Translation(Vector3(0, 0, 0)));

	rootNode->AddChild(knight);
	rootNode->AddChild(sword);
	rootNode->AddChild(Newcastle);
	rootNode->AddChild(tree);

	if (!sceneShader->LinkProgram() || 
		!shadowShader->LinkProgram() || 
		!skyboxShader->LinkProgram() ||
		!terrainShader->LinkProgram() ||
		!waterShader->LinkProgram() ||
		!minimapShader->LinkProgram() ||
		!snowShader->LinkProgram() ||
		!portalShader->LinkProgram() ||
		!terrain->GetBumpMap() ||
		!terrain->GetTexture() ||
		!waterQuad->GetTexture())
	{
		return;
	}
	glGenTextures(1, &shadowTex);
	glBindTexture(GL_TEXTURE_2D, shadowTex);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D,  GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 
		SHADOWSIZE, SHADOWSIZE, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

	glBindTexture(GL_TEXTURE_2D, 0);
	
	glGenFramebuffers(1, &shadowFBO);
	
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
													GL_TEXTURE_2D, shadowTex, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	glGenTextures(1, &minimapTex);
	glGenTextures(1, &minimapDepth);
	glGenFramebuffers(1, &minimapFBO);
	glBindTexture(GL_TEXTURE_2D, minimapTex);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);


	glBindTexture(GL_TEXTURE_2D, minimapDepth);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, minimapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_TEXTURE_2D, minimapTex, 0);	
	 glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, minimapDepth, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, minimapDepth, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	glGenTextures(1, &portalTex);
	glGenTextures(1, &portalDepth);
	glGenFramebuffers(1, &portalFBO);
	glBindTexture(GL_TEXTURE_2D, portalTex);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);


	glBindTexture(GL_TEXTURE_2D, portalDepth);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, portalFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_TEXTURE_2D, portalTex, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, portalDepth, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, portalDepth, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	SetTextureRepeating(terrain->GetTexture(), true);
	SetTextureRepeating(terrain->GetBumpMap(), true);
	SetTextureRepeating(waterQuad->GetTexture(), true);
	SetTextureRepeating(waterQuad->GetBumpMap(), true);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	projMatrix = Matrix4::Perspective(1.0f, 20000.0f, (float)width / (float)height, 45.0f);
	init = true;
}

void Renderer::DrawSkyBox()
{
	glDepthMask(GL_FALSE);
	SetCurrentShader(skyboxShader);
	glUniform3fv(glGetUniformLocation(currentShader->GetProgram(), "cameraPos"), 1, (float*)&(isMinimapMode ? minimapCamera: camera)->GetPosition());
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "cubeTexNight"), 5);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "cubeTexDay"), 7);
	glUniform1f(glGetUniformLocation(currentShader->GetProgram(), "TForLerp"), TForLerpSkybox);

	TForLerpSkybox += (isNight ? 0.5 : -1.2) * 0.0005;
	if (TForLerpSkybox >= 1)
	{
		isNight = false;
	}
	else if (TForLerpSkybox <= 0)
	{
		isNight = true;
	}
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapNight);
	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapDay);
	glUniform1f(glGetUniformLocation(currentShader->GetProgram(), "offsetX"), offsetXSkybox);
	offsetXSkybox += SKY_BOX_ROTATION_SPEED;
	UpdateShaderMatrices();
	glDisable(GL_CULL_FACE);
	quad->Draw();
	glEnable(GL_CULL_FACE);
	glUseProgram(0);
	glDepthMask(GL_TRUE);
}

Renderer::~Renderer()
{
	glDeleteTextures(1, &shadowTex);
	glDeleteFramebuffers(1, &shadowFBO);

	glDeleteTextures(1, &minimapTex);
	glDeleteTextures(1, &minimapDepth);
	glDeleteFramebuffers(1, &minimapFBO);

	glDeleteTextures(1, &portalTex);
	glDeleteTextures(1, &portalDepth);
	glDeleteFramebuffers(1, &portalFBO);

	glDeleteTextures(1, &snowflakeTexture);

	delete sceneShader;
	delete shadowShader;
	delete skyboxShader;
	delete terrainShader;
	delete waterShader;
	delete minimapShader;
	delete portalShader;
	delete snowShader;
	delete minimapCamera;
	delete camera;
	delete[] lights;
	delete snowflakeMesh;
	delete[] snowFlakePositions;
	delete[] snowAmbients;
	delete[] snowOffsets;
	delete terrain;
	currentShader = NULL;
}

void Renderer::UpdateScene(float msec)
{
	static int increment = 1;
	static float cur_value = 1000;
	static float counter = 5;

	if ((counter += 0.01) >= 5)
	{
		windOffset = SimpleMath::Random(-2, 2);
		counter = 0;
	}

	float cameraX = camera->GetPosition().x;
	float cameraY = camera->GetPosition().y;
	float cameraZ = camera->GetPosition().z;

	cameraX = SimpleMath::Clamp(cameraX, WORLD_ORIGIN_X - 5000, WORLD_ORIGIN_X + 5000);
	cameraY = SimpleMath::Clamp(cameraY, 50, 5000);
	cameraZ = SimpleMath::Clamp(cameraZ, WORLD_ORIGIN_Z - 5000, WORLD_ORIGIN_Z + 5000);

	camera->SetPosition(Vector3(cameraX, cameraY, cameraZ));

	float cameraYaw = camera->GetYaw();
	float cameraPitch = camera->GetPitch();

	if (camera->GetIsLocked())
	{
		if (abs(cameraX - cameraStatuses[currentCameraStatusIndex].pos.x) <= 100.0f &&
			abs(cameraY - cameraStatuses[currentCameraStatusIndex].pos.y) <= 100.0f &&
			abs(cameraZ - cameraStatuses[currentCameraStatusIndex].pos.z) <= 100.0f)
		{
			currentCameraStatusIndex += 1;
			if (currentCameraStatusIndex >= NUMBER_OF_CAMERA_POINTS)
			{
				currentCameraStatusIndex = 0;
			}
		}

		camera->SetPosition(Vector3(SimpleMath::Lerp(cameraX, cameraStatuses[currentCameraStatusIndex].pos.x, 0.01),
			SimpleMath::Lerp(cameraY, cameraStatuses[currentCameraStatusIndex].pos.y, 0.01),
			SimpleMath::Lerp(cameraZ, cameraStatuses[currentCameraStatusIndex].pos.z, 0.01)));

		camera->SetYaw(SimpleMath::Lerp(cameraYaw, cameraStatuses[currentCameraStatusIndex].yaw, 0.01));
		camera->SetPitch(SimpleMath::Lerp(cameraPitch, cameraStatuses[currentCameraStatusIndex].pitch, 0.01));
	}
	if (cur_value > 2000)
	{
		increment = -1;
	}
	if (cur_value < -2000)
	{
		increment = 1;
	}
	cur_value += 5 * increment;

	SHADOW_LIGHT.SetPosition(Vector3(WORLD_ORIGIN_X + cur_value, WORLD_ORIGIN_Y + 700, WORLD_ORIGIN_Z + 500));
	rootNode->Update(msec);
	castleRoom->Update(msec);
	camera->UpdateCamera(msec);
	minimapCamera->UpdateCamera(msec);
	viewMatrix = camera->BuildViewMatrix();
	frameFrustum.FromMatrix(projMatrix * viewMatrix);

	if (!isWorld1 && (camera->GetPosition() - PORTAL_ENTRY_POINT).Length() <= 300)
	{
		isWorld1 = true;
		camera->SetPosition(Vector3(4264.97, 1000, -904));
		camera->SetPitch(7.35659);
		camera->SetYaw(102.294);
	}
}

void Renderer::RenderScene()
{
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	DrawSkyBox();
	DrawShadowScene();
    DrawCombinedScene();
	DrawWater();
	if (showMinimap)
	{
		DrawMinimap();
		AttatchMinimap();
	}
	DrawSnow();
	if (!isWorld1)
	{
		DrawPortal();
		AttatchPortal();
	}
	SwapBuffers();
}

void Renderer::DrawShadowScene()
{
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);

	glClear(GL_DEPTH_BUFFER_BIT);
	glCullFace(GL_FRONT);

	glViewport(0, 0, SHADOWSIZE, SHADOWSIZE);

	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	SetCurrentShader(shadowShader);

	viewMatrix = Matrix4::BuildViewMatrix(SHADOW_LIGHT.GetPosition(), Vector3(0, 0, 0));
	textureMatrix = biasMatrix * (projMatrix * viewMatrix);

	UpdateShaderMatrices();
	
	if (!isWorld1)
	{
		terrain->Draw();
		DrawNode(rootNode);
	}
	else
	{
		DrawNode(castleRoom);
	}
	glUseProgram(0);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glViewport(0, 0, width, height);
	glCullFace(GL_BACK);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::DrawCombinedScene()
{
	static float heightOffset = 0;
	SetCurrentShader(sceneShader);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "bumpTex"), 1);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "shadowTex"), 3);
	glUniform1f(glGetUniformLocation(currentShader->GetProgram(), "heightOffset"), heightOffset = SimpleMath::Clamp(heightOffset + 0.0005, 0, 1));
	glUniform3fv(glGetUniformLocation(currentShader->GetProgram(), "cameraPos"), 1, (float*)& (isMinimapMode ? minimapCamera : camera)->GetPosition());

	SetShaderLightGroup(lights, NUMBER_OF_LIGHTS, Vector4(SimpleMath::Lerp(.8, 1, TForLerpSkybox), SimpleMath::Lerp(.8, 1, TForLerpSkybox), 1, 1), AMBIENT_INTENSITY + (TForLerpSkybox * 0.5f), SPECULAR_INTENSITY);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, shadowTex);

	viewMatrix = (isMinimapMode ? minimapCamera : camera)->BuildViewMatrix();
	UpdateShaderMatrices();
	
	if (!isWorld1)
	{
		terrain->Draw();
		DrawNode(rootNode);
	}
	else
	{
		DrawNode(castleRoom);
	}
	glActiveTexture(GL_TEXTURE0);
	glUseProgram(0);
}

void Renderer::DrawNode(SceneNode* n)
{
	if (n->GetMesh())
	{
		Matrix4 transform = n->GetWorldTransform() * Matrix4::Scale(n->GetModelScale());
		glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(), "modelMatrix"), 1, false, (float*)&transform);

		glUniform4fv(glGetUniformLocation(currentShader->GetProgram(), "nodeColor"), 1, (float*)&n->GetColor());

		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "useTexture"), (int)n->GetMesh()->GetTexture());
		n->Draw(*this);
	}

	for (vector<SceneNode*>::const_iterator i = n->GetChildIteratorStart();
		i != n->GetChildIteratorEnd();
		i++)
	{
		DrawNode(*i);
	}
}



void Renderer::DrawWater()
{
	static float offsetX = 0.0f;
	static float waterOffsetX = 0.0f;

	SetCurrentShader(waterShader);
	SetShaderLight(SHADOW_LIGHT);

	glUniform3fv(glGetUniformLocation(currentShader->GetProgram(), "cameraPos"), 1, (float*)& (isMinimapMode ? minimapCamera : camera)->GetPosition());
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "cubeTexNight"), 5);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "cubeTexDay"), 7);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "bumpTex"), 6);
	glUniform1f(glGetUniformLocation(currentShader->GetProgram(), "TForLerp"), TForLerpSkybox);

	if (!isMinimapMode)
	{
		offsetX += WATER_SPEED;
		waterOffsetX += SKY_BOX_ROTATION_SPEED;
	}
	glUniform1f(glGetUniformLocation(currentShader->GetProgram(), "offsetX"), offsetX);
	glUniform1f(glGetUniformLocation(currentShader->GetProgram(), "waterOffsetX"), waterOffsetX);
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapNight);
	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, waterQuad->GetBumpMap());
	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapDay);

	float heightX = (RAW_WIDTH * HEIGHTMAP_X) / 2.0f;
	float heightY = 10;
	float heightZ = (RAW_HEIGHT * HEIGHTMAP_Z / 2.0f);

	modelMatrix = Matrix4::Translation(Vector3(heightX, heightY, heightZ)) *
		Matrix4::Scale(Vector3(heightX * 2, 1, heightZ* 2)) *
		Matrix4::Rotation(90, Vector3(1.0f, 0.0f, 0.0f));

	textureMatrix = Matrix4::Scale(Vector3(10.0f, 10.0f, 10.0f)) * Matrix4::Rotation(0, Vector3(0.0f, 0.0f, 1.0f));
	if (isMinimapMode)
	{
		viewMatrix = minimapCamera->BuildViewMatrix();
	}
	else
	{
		viewMatrix = camera->BuildViewMatrix();
	}
	UpdateShaderMatrices();

	waterQuad->Draw();

	viewMatrix = camera->BuildViewMatrix();
	glUseProgram(0);
	modelMatrix.ToIdentity();
}

void Renderer::DrawMinimap()
{
	glBindFramebuffer(GL_FRAMEBUFFER, minimapFBO);
	isMinimapMode = true;

	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);



	DrawSkyBox();	
	DrawCombinedScene();
	DrawWater();

	isMinimapMode = false;
	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	viewMatrix = camera->BuildViewMatrix();
}

void Renderer::AttatchMinimap()
{ 
	SetCurrentShader(minimapShader);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);

	minimapQuad->SetTexture(minimapTex);

	float heightX = height;
	float heightY = 10;
	float heightZ = (RAW_HEIGHT * HEIGHTMAP_Z / 2.0f);

	modelMatrix = Matrix4::Translation(Vector3(0.7,-0.7,0)) *
		Matrix4::Scale(Vector3(0.3, 0.3, 0.3));
	projMatrix = Matrix4::Orthographic(-1, 1, 1, -1, -1, 1);
	viewMatrix.ToIdentity();

	textureMatrix.ToIdentity();
	UpdateShaderMatrices();
	minimapQuad->Draw();
	glUseProgram(0);
	modelMatrix.ToIdentity();
	textureMatrix.ToIdentity();
	projMatrix = Matrix4::Perspective(1.0f, 15000.0f, (float)width / (float)height, 45.0f);
	viewMatrix = camera->BuildViewMatrix();
}

void Renderer::DrawSnow()
{
	glDepthMask(GL_FALSE);
	static float sinOffset  = 0.0f;

	SetCurrentShader(snowShader);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);

	float heightX = 20;
	float heightY = 20;
	float heightZ = 20;
	viewMatrix = camera->BuildViewMatrix();
	frameFrustum.FromMatrix(projMatrix * viewMatrix);
	int counter = 0;
	for (int i = 0; i < NUMBER_OF_PARTICLES; i++)
	{
		SceneNode node;
		node.SetBoundingRadius(20);
		snowFlakePositions[i] += snowOffsets[i];
		if (snowFlakePositions[i].y < -100)
		{
			float offsetX = SimpleMath::Random(-SNOW_OFFSET, SNOW_OFFSET);
			float offsetZ = SimpleMath::Random(-SNOW_OFFSET, SNOW_OFFSET);
			float offsetY = SimpleMath::Random(7000, 8000);
			snowFlakePositions[i] = Vector3(WORLD_ORIGIN_X + offsetX, offsetY, WORLD_ORIGIN_Z + offsetZ);
			snowColors[i] = Vector4(SimpleMath::Random(0.5, 1), SimpleMath::Random(0.5, 1), SimpleMath::Random(0.5, 1), 1);
			snowOffsets[i] = Vector3(SimpleMath::Random(-2, 2),
				SimpleMath::Random(SNOW_SPEED, -2),
				SimpleMath::Random(-2, 2));
		}
		node.SetTransform(Matrix4::Translation(snowFlakePositions[i]) *
							Matrix4::Scale(Vector3(heightX, heightY, heightZ)) *
							viewMatrix.GetTransposedRotation());
		node.Update(1);
		if (!frameFrustum.InsideFrustum(node))
		{
			continue;
		}
		counter++;
		modelMatrix = Matrix4::Translation(snowFlakePositions[i]) *
			 Matrix4::Scale(Vector3(heightX, heightX, heightX)) * 
			viewMatrix.GetTransposedRotation();
		textureMatrix.ToIdentity();
		UpdateShaderMatrices();

		glUniform4fv(glGetUniformLocation(currentShader->GetProgram(), "snowColor"),1,(float*)&snowColors[i]);
		glUniform1f(glGetUniformLocation(currentShader->GetProgram(), "snowAmbient"), snowAmbients[i].ambient += (0.005f * (snowAmbients[i].isIncreasing ? 1 : -1)));
		if (snowAmbients[i].ambient >= 0.25)
		{
			snowAmbients[i].isIncreasing = false;
		}
		else if (snowAmbients[i].ambient <= -0.2)
		{
			snowAmbients[i].isIncreasing = true;
		}
		glDisable(GL_CULL_FACE);
		snowflakeMesh->Draw();
		glEnable(GL_CULL_FACE);
	}
	glUseProgram(0);
	modelMatrix.ToIdentity();
	textureMatrix.ToIdentity();
	glDepthMask(GL_TRUE);
}

void Renderer::DrawPortal()
{
	glBindFramebuffer(GL_FRAMEBUFFER, portalFBO);

	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	viewMatrix = portalCamera.BuildViewMatrix();
	castleRoom->SetTransform(Matrix4::Translation(Vector3(0, 0, 0)) * viewMatrix.GetTransposedRotation());
	DrawSkyBox();

	SetCurrentShader(portalShader);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);
	viewMatrix = portalCamera.BuildViewMatrix();
	UpdateShaderMatrices();
	DrawNode(castleRoom);
	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::AttatchPortal()
{
	SetCurrentShader(portalShader);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);

	portalQuad->SetTexture(portalTex);
	
	modelMatrix = Matrix4::Translation(WORLD_ORIGIN_POINT + Vector3(1000, 1000, -1500)) *
		Matrix4::Scale(Vector3(250, 250, 250)) *
		Matrix4::Rotation(180, Vector3(0, 0, 1));
	viewMatrix = camera->BuildViewMatrix();

	textureMatrix.ToIdentity();
	UpdateShaderMatrices();
	glDisable(GL_CULL_FACE);
	portalQuad->Draw();
	glEnable(GL_CULL_FACE);
	glUseProgram(0);

	modelMatrix.ToIdentity();
	textureMatrix.ToIdentity();
}
