#include "GroundNode.h"

GroundNode::GroundNode()
{
	OBJMesh* ground_obj = new OBJMesh();
	ground_obj->LoadOBJMesh(MESHDIR"monu9-2.obj");
	mesh = ground_obj;
	mesh->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"monu9.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
}

GroundNode::~GroundNode()
{
}

void GroundNode::Update(float msec)
{
	SceneNode::Update(msec);
}