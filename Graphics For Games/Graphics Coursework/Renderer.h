#pragma once

#include "../../nclgl/OGLRenderer.h"
#include "../../nclgl/Camera.h"
#include "../../nclgl/MD5Mesh.h"
#include "../../nclgl/MD5Node.h"
#include "../../nclgl/OBJMesh.h"
#include "../../nclgl/HeightMap.h"
#include "../../nclgl/Frustum.h"

#include "RootNode.h"

#include "GroundNode.h"
#include "KnightNode.h"
#include "SwordNode.h"
#include "CastleNode.h"
#include "TreeNode.h"
#include "CastleRoomNode.h"
#include "SimpleMath.h"

#define SHADOW_LIGHT lights[0]
#define WORLD_ORIGIN_POINT Vector3(WORLD_ORIGIN_X, WORLD_ORIGIN_Y, WORLD_ORIGIN_Z)
#define PORTAL_ENTRY_POINT Vector3(5052,860,2616) 

constexpr int  WORLD_ORIGIN_X = 4096;
constexpr int  WORLD_ORIGIN_Y = 0;
constexpr int  WORLD_ORIGIN_Z = 4096;

constexpr auto SHADOWSIZE = 4096;
constexpr float SKY_BOX_ROTATION_SPEED = 0.0005f;
constexpr float WATER_SPEED = 0.001f;
constexpr int NUMBER_OF_LIGHTS = 2;

constexpr float AMBIENT_INTENSITY = 0.2f;
constexpr float SPECULAR_INTENSITY = 0.3f;

constexpr int NUMBER_OF_CAMERA_POINTS = 5;

constexpr int NUMBER_OF_PARTICLES = 10000;

constexpr float SNOW_SPEED = -7;
constexpr float SNOW_OFFSET = 7000;

struct CameraStatus
{
	float yaw;
	float pitch;

	Vector3 pos;
	CameraStatus(float y = 0, float p = 0, Vector3 pos = Vector3(0, 0, 0)) : yaw(y), pitch(p), pos(pos)
	{

	}
};

struct SnowAmbient
{
	float ambient;
	bool isIncreasing;
};

class Renderer : public OGLRenderer
{
public:
	Renderer(Window& parent);
	virtual ~Renderer();

	virtual void UpdateScene(float msec);
	virtual void RenderScene();

	float getCameraYaw()
	{
		return camera->GetYaw();
	}

	float getCameraPitch()
	{
		return camera->GetPitch();
	}

	Vector3 getCameraPos()
	{
		return camera->GetPosition();
	}

	void LockCamera()
	{
		camera->SetIsLocked(true);
	}
	void UnlockCamera()
	{
		camera->SetIsLocked(false);
	}

	void SpaceJump(int index)
	{
		index = SimpleMath::Clamp(index, 0, NUMBER_OF_CAMERA_POINTS - 1);
		currentCameraStatusIndex = index;
		camera->SetCameraStatus(cameraStatuses[index].yaw, cameraStatuses[index].pitch, cameraStatuses[index].pos);
	}

	void ReturnToWorld0()
	{
		isWorld1 = false;
		camera->SetPosition(Vector3(WORLD_ORIGIN_X, 1000, WORLD_ORIGIN_Z));
	}

	void SetShowMinimap(bool val)
	{
		showMinimap = val;
	}
protected:
	const CameraStatus const cameraStatuses[NUMBER_OF_CAMERA_POINTS] =
	{
		CameraStatus(230, -40, Vector3(-904, 5000, -904)),
		CameraStatus(14, -30, Vector3(3879, 2194, 6570)),
		CameraStatus(45, -22, Vector3(9096, 2794, 9096)),
		CameraStatus(-47, -21, Vector3(-904, 2427, 9096)),
		CameraStatus(138, -3, Vector3(9096, 659, -904))
	};

	void DrawShadowScene();
	void DrawCombinedScene();
	void DrawNode(SceneNode* n);
	void DrawSkyBox();
	void DrawWater();
	void DrawMinimap();
	void AttatchMinimap();
	void DrawSnow();
	void DrawPortal();
	void AttatchPortal();

	Shader* sceneShader;
	Shader* shadowShader;

	GLuint shadowTex;
	GLuint shadowFBO;

	GLuint minimapTex;
	GLuint minimapDepth;
	GLuint minimapFBO;

	GLuint portalTex;
	GLuint portalDepth;
	GLuint portalFBO;

	SceneNode* rootNode;

	SceneNode* castleRoom;
	SceneNode* knight;
	SceneNode* sword;
	SceneNode* Newcastle;
	SceneNode* tree;

	Camera* camera;
	Camera* minimapCamera;
	Camera portalCamera = Camera(0, 120, Vector3(WORLD_ORIGIN_X - 800, WORLD_ORIGIN_Y + 1700, WORLD_ORIGIN_Z - 6000));
	Light* lights; // Only light[0] casts shadows.

	Mesh* quad;	
	Mesh* waterQuad;
	Mesh* minimapQuad;
	Mesh* portalQuad;
	Shader* skyboxShader;
	GLuint cubeMapNight;
	GLuint cubeMapDay;

	HeightMap* terrain;
	Shader* terrainShader;

	Shader* waterShader;

	Shader* minimapShader;

	Shader* portalShader;

	int currentCameraStatusIndex = 0;

	bool isMinimapMode = false;

	float offsetXSkybox = 0;
	float TForLerpSkybox = 0;
	bool isNight = true;

	Mesh* snowflakeMesh;
	GLuint snowflakeTexture;
	Shader* snowShader;
	SnowAmbient* snowAmbients;

	Vector3* snowFlakePositions;
	Vector4* snowColors;
	Vector3* snowOffsets;

	Frustum frameFrustum;

	float windOffset = 0;

	bool isWorld1 = false;

	bool showMinimap = true;
};

