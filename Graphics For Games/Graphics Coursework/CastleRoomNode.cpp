#include "CastleRoomNode.h"

CastleRoomNode::CastleRoomNode()
{
	OBJMesh* castleRoom_obj = new OBJMesh();
	castleRoom_obj->LoadOBJMesh(MESHDIR"InsideCastle.obj");
	castleRoom_obj->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"InsideCastle.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
	mesh = castleRoom_obj;
}

CastleRoomNode::~CastleRoomNode()
{
}

void CastleRoomNode::Update(float msec)
{
	SceneNode::Update(msec);
}
