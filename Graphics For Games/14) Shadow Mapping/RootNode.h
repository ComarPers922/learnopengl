#pragma once
#include "..\..\nclgl\SceneNode.h"
#include "..\..\nclgl\OBJMesh.h"
#include "GroundNode.h"

class RootNode: public SceneNode
{
public:
	RootNode();
	~RootNode();

	virtual void Update(float msec);

protected:
	GroundNode* rootMark;
};

