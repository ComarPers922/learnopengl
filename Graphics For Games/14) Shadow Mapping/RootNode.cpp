#include "RootNode.h"

RootNode::RootNode()
{
	rootMark = new GroundNode();
	
	SetTransform(Matrix4::Translation(Vector3(4096, 0, 4096)));
	AddChild(rootMark);
	rootMark->SetTransform(Matrix4::Translation(Vector3(0, 20, 0)));
	rootMark->SetModelScale(Vector3(100, 100, 100));
}

RootNode::~RootNode()
{
}

void RootNode::Update(float msec)
{
	SceneNode::Update(msec);
	rootMark->Update(msec);
	for (auto item = GetChildIteratorStart(); item != GetChildIteratorEnd(); item++)
	{
		(*item)->Update(msec);
	}
}
