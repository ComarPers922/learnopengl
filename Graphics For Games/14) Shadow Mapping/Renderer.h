#pragma once

#include "../../nclgl/OGLRenderer.h"
#include "../../nclgl/Camera.h"
#include "../../nclgl/MD5Mesh.h"
#include "../../nclgl/MD5Node.h"
#include "../../nclgl/OBJMesh.h"
#include "../../nclgl/HeightMap.h"

#include "RootNode.h"

#include "GroundNode.h"
#include "KnightNode.h"
#include "SwordNode.h"
#include "CastleNode.h"

#define SHADOW_LIGHT lights[0]

constexpr int  WORLD_ORIGIN_X = 4096;
constexpr int  WORLD_ORIGIN_Y = 0;
constexpr int  WORLD_ORIGIN_Z = 4096;

constexpr auto SHADOWSIZE = 4096;
constexpr float SKY_BOX_ROTATION_SPEED = 0.0005f;
constexpr int NUMBER_OF_LIGHTS = 2;

class Renderer : public OGLRenderer
{
public:
	Renderer(Window& parent);
	virtual ~Renderer();

	virtual void UpdateScene(float msec);
	virtual void RenderScene();

protected:
	void DrawMesh();
	void DrawFloor();
	void DrawShadowScene();
	void DrawCombinedScene();
	void DrawNode(SceneNode* n);

	Shader* sceneShader;
	Shader* shadowShader;

	GLuint shadowTex;
	GLuint shadowFBO;

	GroundNode* ground;

	SceneNode* rootNode;

	SceneNode* knight;
	SceneNode* sword;
	SceneNode* Newcastle;

	MD5FileData* hellData;
	MD5Node* hellNode;
	Mesh* floor;
	Camera* camera;
	Light* lights; // Only light[0] casts shadows.

	Mesh* quad;	
	Shader* skyboxShader;
	GLuint cubeMapNight;

	HeightMap* terrain;
	Shader* terrainShader;

	Shader* shadowLightShader;

	void DrawSkyBox();
	void DrawTerrain();
};

