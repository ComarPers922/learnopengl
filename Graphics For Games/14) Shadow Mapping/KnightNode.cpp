#include "KnightNode.h"


KnightNode::KnightNode()
{
	OBJMesh* knight_obj = new OBJMesh();
	knight_obj->LoadOBJMesh(MESHDIR"chr_knight-2.obj");
	knight_obj->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"chr_knight.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
	mesh = knight_obj;

	SetModelScale(Vector3(20, 20, 20));
	SetTransform(Matrix4::Translation(Vector3(-150, 20, 60.0)));
}

KnightNode::~KnightNode()
{

}

void KnightNode::Update(float msec)
{
	SceneNode::Update(msec);
}