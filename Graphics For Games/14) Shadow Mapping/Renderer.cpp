#include "Renderer.h"

Renderer::Renderer(Window& parent) : OGLRenderer(parent)
{
	quad = Mesh::GenerateQuad();

	terrain = new HeightMap(TEXTUREDIR"terrain512.raw");
	terrain->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"lowpolyterrain.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));

	terrain->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"Barren RedsDOT3.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	
	camera = new Camera(-8.0f, 40.0f, Vector3(-200.0f, 50.0f, 250.0f), 2.0f);// 0.1f);
	lights = new Light[NUMBER_OF_LIGHTS];
	SHADOW_LIGHT =  Light(Vector3(WORLD_ORIGIN_X, WORLD_ORIGIN_Y + 1000, WORLD_ORIGIN_Z), Vector4(1, 0.9568627f, 0.8392157f, 1), 5500.0f, 0.7f);
	lights[1] = Light(Vector3(WORLD_ORIGIN_X, WORLD_ORIGIN_Y + 1000, WORLD_ORIGIN_Z), Vector4(1, .5, .5, 1), 5500.0f, 0.2f);

	hellData = new MD5FileData(MESHDIR"hellknight.md5mesh");
	hellNode = new MD5Node(*hellData);
	
	hellData->AddAnim(MESHDIR"idle2.md5anim");
	hellNode->PlayAnim(MESHDIR"idle2.md5anim");

	sceneShader = new Shader(SHADERDIR"shadowlightshaderVert.glsl",
													SHADERDIR"shadowlightshaderFrag.glsl");
	shadowShader = new Shader(SHADERDIR"shadowVert.glsl",
															SHADERDIR"shadowFrag.glsl");
	skyboxShader = new Shader(SHADERDIR"SkyboxVertex.glsl",
															SHADERDIR"SkyboxFragment.glsl");
	terrainShader = new Shader(SHADERDIR"BumpVertex.glsl", 
															SHADERDIR"BumpFragment.glsl");

	cubeMapNight = SOIL_load_OGL_cubemap(TEXTUREDIR"CosmicCoolCloudLeft.tga", TEXTUREDIR"CosmicCoolCloudRight.tga",
		TEXTUREDIR"CosmicCoolCloudBottom.tga", TEXTUREDIR"CosmicCoolCloudTop.tga",
		TEXTUREDIR"CosmicCoolCloudBack.tga", TEXTUREDIR"CosmicCoolCloudFront.tga",
		SOIL_LOAD_RGBA, SOIL_CREATE_NEW_ID, 0);

	rootNode = new RootNode();

	knight = new KnightNode();
	sword = new SwordNode();
	Newcastle = new CastleNode();



	knight->SetModelScale(Vector3(50, 50, 50));
	knight->SetTransform(Matrix4::Translation(Vector3(-900, 60, -300)));
	sword->SetModelScale(Vector3(50, 50, 50));
	sword->SetTransform(Matrix4::Translation(Vector3(-1000, 60, -300)));
	Newcastle->SetModelScale(Vector3(500, 500, 500));
	Newcastle->SetTransform(Matrix4::Translation(Vector3(-1000, 420, -1500)));

	rootNode->AddChild(knight);
	rootNode->AddChild(sword);
	rootNode->AddChild(Newcastle);

	if (!sceneShader->LinkProgram() || 
		!shadowShader->LinkProgram() || 
		!skyboxShader->LinkProgram() ||
		!terrainShader->LinkProgram() ||
		!terrain->GetBumpMap() ||
		!terrain->GetTexture())
	{
		return;
	}
	glGenTextures(1, &shadowTex);
	glBindTexture(GL_TEXTURE_2D, shadowTex);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D,  GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 
		SHADOWSIZE, SHADOWSIZE, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

	glBindTexture(GL_TEXTURE_2D, 0);
	
	glGenFramebuffers(1, &shadowFBO);
	
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
													GL_TEXTURE_2D, shadowTex, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	floor = Mesh::GenerateQuad();
	floor->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"lowpolyterrain.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	// floor->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"brickDOT3.tga", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	SetTextureRepeating(terrain->GetTexture(), true);
	SetTextureRepeating(terrain->GetBumpMap(), true);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	projMatrix = Matrix4::Perspective(1.0f, 15000.0f, (float)width / (float)height, 45.0f);
	init = true;
}
void Renderer::DrawSkyBox()
{
	static float offsetX = 0;
	glDepthMask(GL_FALSE);
	SetCurrentShader(skyboxShader);
	glUniform3fv(glGetUniformLocation(currentShader->GetProgram(), "cameraPos"), 1, (float*)&camera->GetPosition());
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "cubeTex"), 5);
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapNight);
	glUniform1f(glGetUniformLocation(currentShader->GetProgram(), "offsetX"), offsetX);
	offsetX += SKY_BOX_ROTATION_SPEED;
	UpdateShaderMatricesG();
	glDisable(GL_CULL_FACE);
	quad->Draw();
	glEnable(GL_CULL_FACE);
	glUseProgram(0);
	glDepthMask(GL_TRUE);
}
void Renderer::DrawTerrain()
{
	SetCurrentShader(terrainShader);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "bumpTex"), 1);

	glUniform3fv(glGetUniformLocation(currentShader->GetProgram(), "cameraPos"), 1, (float*)&camera->GetPosition());

	UpdateShaderMatrices();
	SetShaderLight(*lights);
	terrain->Draw();
	glUseProgram(0);
}
Renderer::~Renderer()
{
	glDeleteTextures(1, &shadowTex);
	glDeleteFramebuffers(1, &shadowFBO);

	delete camera;
	delete[] lights;
	delete hellData;
	delete hellNode;
	delete floor;
	delete terrain;
	delete sceneShader;
	delete shadowShader;
	currentShader = NULL;
}

void Renderer::UpdateScene(float msec)
{
	rootNode->Update(msec);
	camera->UpdateCamera(msec);
	hellNode->Update(msec);
	viewMatrix = camera->BuildViewMatrix();
}

void Renderer::RenderScene()
{
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	DrawSkyBox();
	// DrawTerrain();
	DrawShadowScene();
    DrawCombinedScene();

	SwapBuffers();
}

void Renderer::DrawShadowScene()
{
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);

	glClear(GL_DEPTH_BUFFER_BIT);

	glViewport(0, 0, SHADOWSIZE, SHADOWSIZE);

	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	SetCurrentShader(shadowShader);

	viewMatrix = Matrix4::BuildViewMatrix(SHADOW_LIGHT.GetPosition(), Vector3(0, 0, 0));
	textureMatrix = biasMatrix * (projMatrix * viewMatrix);

	UpdateShaderMatrices();
	terrain->Draw();
	DrawNode(rootNode);
	glUseProgram(0);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glViewport(0, 0, width, height);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::DrawCombinedScene()
{
	SetCurrentShader(sceneShader);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "bumpTex"), 1);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "shadowTex"), 3);

	glUniform3fv(glGetUniformLocation(currentShader->GetProgram(), "cameraPos"), 1, (float*)&camera->GetPosition());

	SetShaderLightGroup(lights, NUMBER_OF_LIGHTS, Vector4(1, 1, 1, 1), 0.3f, 0.3f);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, shadowTex);

	viewMatrix = camera->BuildViewMatrix();
	UpdateShaderMatrices();
	terrain->Draw();
	DrawNode(rootNode);
	glActiveTexture(GL_TEXTURE0);
	glUseProgram(0);
}

void Renderer::DrawMesh()
{
	modelMatrix.ToIdentity();

	Matrix4 tempMatrix = textureMatrix * modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(), "textureMatrix"),1, false, *&tempMatrix.values);
	glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(), "modelMatrix"), 1, false, *&modelMatrix.values);
	hellNode->Draw(*this);
}

void Renderer::DrawFloor()
{
	modelMatrix = Matrix4::Rotation(90, Vector3(1, 0, 0)) * Matrix4::Scale(Vector3(450, 450, 1));
	Matrix4 tempMatrix = textureMatrix * modelMatrix;

	glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(), "textureMatrix"), 1, false, *&tempMatrix.values);
	glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(), "modelMatrix"), 1, false, *&modelMatrix.values);
	floor->Draw();
}

void Renderer::DrawNode(SceneNode* n)
{
	if (n->GetMesh())
	{
		Matrix4 transform = n->GetWorldTransform() * Matrix4::Scale(n->GetModelScale());
		glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(), "modelMatrix"), 1, false, (float*)&transform);

		glUniform4fv(glGetUniformLocation(currentShader->GetProgram(), "nodeColor"), 1, (float*)&n->GetColor());

		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "useTexture"), (int)n->GetMesh()->GetTexture());
		n->Draw(*this);
	}

	for (vector<SceneNode*>::const_iterator i = n->GetChildIteratorStart();
		i != n->GetChildIteratorEnd();
		i++)
	{
		DrawNode(*i);
	}
}