
#include "../../NCLGL/window.h"
#include "Renderer.h"

#pragma comment(lib, "nclgl.lib")

int main() {
	Window w("My Recreational Code", 1280, 960, false);
	if (!w.HasInitialized()) {
		return -1;
	}

	Renderer renderer(w);//This handles all the boring OGL 3.2 initialization stuff, and sets up our tutorial!
	if (!renderer.HasInitialized()) {
		return -1;
	}

	w.LockMouseToWindow(true);
	w.ShowOSPointer(true);

	while (w.UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE)) {
		renderer.UpdateScene(w.GetTimer()->GetTimedMS());
		renderer.RenderScene();
	}

	return 0;
}