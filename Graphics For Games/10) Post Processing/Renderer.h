#pragma once

#include "../../nclgl/OGLRenderer.h"
#include "../../nclgl/HeightMap.h"
#include "../../nclgl/Camera.h"

constexpr auto POST_PASSES = 1;

class Renderer : public OGLRenderer
{
public:
	Renderer(Window& parent);
	virtual ~Renderer();

	virtual void RenderScene();
	virtual void UpdateScene(float msec);

protected:
	void PresentScene();
	void DrawPostProcess();
	void DrawScene();

	Shader* sceneShader;
	Shader* processShader;

	Camera* camera;

	Mesh* quad;
	HeightMap* heightMap;

	GLuint bufferFBO;
	GLuint processFBO;
	GLuint bufferColorTex[2];
	GLuint bufferDepthTex;
};

